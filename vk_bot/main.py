'''
https://vk.com/dev/bots_longpoll
'''

import requests
import json
import hashlib
import random
from configs import *

keyboard = {
    "one_time": False,
    "buttons": [
        [
            {
                "action": {
                    "type": "text",
                    "payload": "{\"button\": \"2\"}",
                    "label": "Граф друзей"
                },
                "color": "positive"
            }
        ]
    ]
}


def send_message(user_id, text, keyboard=None):
    params = {'access_token': access_token,
              'random_id': random.randint(0, 1e10),
              'group_id': group_id,
              'user_id': user_id,
              'message': text,
              'v': api_version}
    if keyboard is not None:
        params['keyboard'] = json.dumps(keyboard, ensure_ascii=False).encode('utf8')

    response = requests.get('https://api.vk.com/method/messages.send', params=params).json()
    if 'error' in response:
        print(response)


params = {'access_token': access_token,
          'group_id': group_id,
          'v': api_version}
response = requests.get('https://api.vk.com/method/groups.getLongPollServer', params=params).json()
if 'error' in response:
    print(response)

key, server, ts = response['response']['key'], response['response']['server'], response['response']['ts']

while True:
    params = {'act': 'a_check',
              'key': key,
              'ts': ts,
              'wait': 0.1}
    response = requests.get(server, params=params).json()
    if 'error' in response:
        print(response)

    ts = response['ts']

    for update in response['updates']:
        if update['type'] == 'message_new':
            message = update['object']['body']
            user_id = update['object']['user_id']

            if message == 'Граф друзей':
                message = f'https://oauth.vk.com/authorize?client_id={app_id}&display=page&redirect_uri=http://vk-infographics.ml/OAuth/&scope=friends,offline&response_type=code&v={api_version}'
                send_message(user_id, 'Разрешите нам увидеть ваших друзей.', keyboard)
                send_message(user_id, f'Ссылка на социальный граф: {message}', keyboard)
                send_message(user_id, 'Поделитесь своим графом на стене в группе!', keyboard)

            else:
                send_message(user_id, 'Привет!', keyboard)
                send_message(user_id, 'Это приложение позволяет вам увидеть и исследовать социальную структуру людей, с которыми вы дружите в ВКонтакте.', keyboard)
                send_message(user_id, 'Для продолжения нажмите \"Граф друзей\"', keyboard)