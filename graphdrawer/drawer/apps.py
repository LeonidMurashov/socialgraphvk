from django.apps import AppConfig


class DrawerConfig(AppConfig):
    name = 'drawer'
