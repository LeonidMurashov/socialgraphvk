from django.shortcuts import render
import requests
import json
import hashlib
from . import configs
import os
import sys
from django.http import HttpResponseNotFound


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_user_data(user_hash):
    with open(configs.database_path, 'r') as f:
        data = json.load(f)
        user_data = data.get(user_hash)

    return user_data


def size_from_friends_count(n):
    return max(6, 10 * n ** 0.5)


def index(request):
    if request.method == 'GET':

        user_hash = request.GET.get('user_hash')
        print('Processing', user_hash)

        user_data = get_user_data(user_hash)
        if user_data is None:
            return HttpResponseNotFound('<h1>User info not found</h1>')

        user = user_data['user']
        friends = user_data['friends']['items']
        links = user_data['links']

        print(user)

        index_to_username = {item['id']: f'{item["last_name"]} {item["first_name"]}' for item in friends}
        index_to_username[user['id']] = f'{user["last_name"]} {user["first_name"]}'

        id_to_friends_count = {item['id']: item['common_count'] for item in links}
        id_to_friends_count[user['id']] = len(friends)
        vertices = [
            {
                'name': f'{item["last_name"]} {item["first_name"]}',
                'photo': item['photo_50'],
                'size': size_from_friends_count(id_to_friends_count[item['id']]),
                'vklink': 'https://vk.com/id' + str(item['id'])
            }
            for item in friends
            if 'DELETED' not in [item['last_name'], item['first_name']] and
               item['photo_50'] != 'https://vk.com/images/deactivated_50.png'
        ]
        vertices.append({'name': f'{user["last_name"]} {user["first_name"]}',
                         'photo': user['photo_50'],
                         'size': size_from_friends_count(len(friends)),
                         'vklink': 'https://vk.com/id' + str(user['id'])})

        deleted = [item['id']
                   for item in friends
                   if 'DELETED' in [item['last_name'], item['first_name']] or
                   item['photo_50'] == 'https://vk.com/images/deactivated_50.png'
                   ]

        edges_indices_to_each_other = {
            tuple(sorted([user_friends['id'], user_friend]))
            for user_friends in links
            for user_friend in user_friends['common_friends']
        }
        edges_indices_to_user = {(user['id'], vertex['id']) for vertex in friends}
        edges_indices = edges_indices_to_each_other | edges_indices_to_user

        edges_indices = list(filter(lambda e: e[0] not in deleted and e[1] not in deleted, edges_indices))

        edges = [(index_to_username[user1], index_to_username[user2]) for user1, user2 in edges_indices]

        print('Finished.')

        return render(request,
                      'drawer/graph.html',
                      {'vertices': vertices,
                       'edges': edges})
