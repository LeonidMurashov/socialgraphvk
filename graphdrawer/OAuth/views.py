from django.shortcuts import render
import requests
import json
import hashlib
from . import configs
import os
import sys
import urllib
from django.http import HttpResponseRedirect


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_unique_value_of_user(user_id):
    return hashlib.md5((user_id + configs.salt_number).encode('utf-8')).hexdigest()


def generate_user_data(user_id, access_token):
    params = {'access_token': access_token,
              'user_ids': user_id,
              'fields': ['nickname', 'photo_50'],
              'v': configs.api_version}
    r = requests.get('https://api.vk.com/method/users.get', params=params)
    # print(r.json())
    user_data = {'user': r.json()['response'][0]}

    params = {'access_token': access_token,
              'fields': ['nickname', 'photo_50'],
              'v': configs.api_version}
    r = requests.get('https://api.vk.com/method/friends.get', params=params)
    user_data['friends'] = r.json()['response']

    # Requesting friends of friends
    friends = [item['id'] for item in r.json()['response']['items']
               if 'deactivated' not in item]

    user_data['links'] = []
    for i in range(0, len(friends), configs.vk_max_mutual_friends_pull):
        params = {'access_token': access_token,
                  'target_uids': ','.join(map(str, friends[i:i + configs.vk_max_mutual_friends_pull])),
                  'source_uid': user_id,
                  'v': configs.api_version}
        r = requests.get('https://api.vk.com/method/friends.getMutual', params=params)
        user_data['links'] += r.json()['response']

    # print('user\t', user_data['user'])
    # print('friends\t', user_data['friends'])
    # print('links\t', user_data['links'])

    with open(configs.database_path, 'r') as f:
        data = json.load(f)

    unique_value = get_unique_value_of_user(user_id)
    data[unique_value] = user_data

    with open(configs.database_path, 'w') as f:
        json.dump(data, f)


def get_user_id_and_access_token(code):
    params = {'client_id': configs.app_id,
              'client_secret': configs.access_token_client_secret,
              'redirect_uri': configs.redirect_uri,
              'code': code,
              'v': configs.api_version}
    response = requests.get('https://oauth.vk.com/access_token/', params=params).json()
    if 'error' in response:
        eprint(response)

    return str(response['user_id']), response['access_token']


def index(request):
    if request.method == 'GET':

        # This is part of OAuth
        code = request.GET.get('code')
        user_id, access_token = get_user_id_and_access_token(code)

        generate_user_data(user_id, access_token)

        params = urllib.parse.urlencode({'user_hash': get_unique_value_of_user(user_id)})
        return HttpResponseRedirect('/drawer' + "?%s" % params)
