# SocialGraphVK

Это репозиторий группы ВКонтакте ["Рисуем социальную инфографику ВК"](https://vk.com/infographicsvk).  

Он включает в себя:
- Python скрипт, отвечающий на сообщения в лс группы,
- бэкенд на Python (standalone приложение ВК), получающий друзей пользователя и связи между ними,
- бэкенд на Python на Django, генерирующий граф из полученных данных.  

Для плоской укладки графа используется библиотека [VivaGraphJS](https://github.com/anvaka/VivaGraphJS).  
Бот и сайт запущенны на удалённом сервере на digitalocean.  

## Как запустить проект самому

Вам потребуется:
- создать группу в ВК, добавить её access_code в vk_bot/group_access_token.secret_data
- создать standalone в ВК, добавить его private key в graphdrawer/app_private_key.secret_data
- сгенерировать Django PRIVATE_KEY и поместить в graphdrawer/secret_key.secret_data
- сгенерировать случайный ключ и поместить в graphdrawer/salt_number.secret_data
- добавить IP сервера в ALLOWED_HOSTS в graphdrawer/graphdrawer/settings.py

Запускаем бэкенд в докер контейнерах:
```
git clone https://gitlab.com/LeonidMurashov/socialgraphvk
cd socialgraphvk
docker-compose build
docker-compose up
```
**Готово!**